\input{preamble}
\usepackage{listings}
\lstset{language=C++,
    basicstyle=\ttfamily,
    keywordstyle=\color{blue}\ttfamily,
    stringstyle=\color{red}\ttfamily,
    commentstyle=\color{ForestGreen}\ttfamily,
    morecomment=[l][\color{magenta}]{\#}
}

\title{Montage d'un fréquencemètre avec des amplificateurs opérationnels et Arduino}
\author{Edgar Bizel \\ Damien Phan \\\\ Polytech Nice Sophia}

\sisetup{range-phrase=\text{ -- }, round-mode=figures, round-precision=2}

\begin{document}
    \maketitle

    \section{Introduction}

    Durant trois séances de travaux pratiques, nous allons réaliser un fréquencemètre à
    l'aide d'amplificateurs opérationnels LM-741 et de la plateforme Arduino.

    Celui-ci sera décomposé en deux parties : la génération du signal, et le traitement du
    signal.

    \section{Réalisation du montage}

    \subsection{Multivibrateur astable}

    \subsubsection{Principes théoriques}%
    
    La première partie du montage est un multivibrateur. Le multivibrateur astable permet de
    générer un signal alternant périodiquement entre deux états. Il produit ainsi un
    signal de la forme suivante :
    \begin{figure}[H]
        \centering
        \includegraphics[width=0.6\textwidth]{2.1_Graph.png}
        \caption{Signal émis par un multivibrateur astable}
    \end{figure}
    Avec :
    \begin{itemize}
        \item $V_{cc}$ la valeur de l'alimentation positive
        \item $V_{th}^+$ le seuil de basculement positif
        \item $V_{th}^-$ le seuil de basculement positif
        \item $-V_{cc}$ la valeur de l'alimentation négative
    \end{itemize}
    Le multivibrateur que nous utilisons repose sur l'utilisateur d'une capacité.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{2.1_Montage.png}
        \caption{Montage d'un multivibrateur}
    \end{figure}
    Son fonctionnement s'explique de la façon suivante :
    \begin{itemize}
        \item L'AOP (amplificateur opérationnel) est utilisé en mode comparateur. Sa
        sortie vaudra donc $+V_{cc}$ ou $-V_{cc}$.

        On suppose qu'à $t=0$, la sortie est $+V_{cc}$.

        \item La sortie étant positive, le condensateur se charge.

        \item Lorsque le condensateur atteint une certaine charge, la tension sur la patte
        $-$, $V^-$, dépasse la tension sur la patte $+$, $V^+$ : cette valeur est appelée $V_{th}^+$. La sortie vaut alors $-V_{cc}$ 
        \item La sortie étant négative, le condensateur se décharge par $R$.

        \item Lorsque le condensateur atteint une certaine charge, $V^+$ dépasse $V^-$:
            cette valeur est appelée $V_{th}^-$. La sortie vaut alors $+V_{cc}$

        \item Ce cycle se répète indéfiniment
    \end{itemize}
    Pour calculer les valeurs de $V_{th}^+$ et $V_{th}^-$, il faut d'abord connaître la
    valeur de $V^+$. 

    Ici, les résistances $R_1$ et $R_2$ forment un diviseur de tension. On a donc $V^+ =
    \frac{R_1}{R_1+R_2} V_s$.

    On a donc :
    \begin{itemize}
        \item $V_{th}^+ = \frac{R_1}{R_1+R_2}V_{cc}$

        \medskip

        \item $V_{th}^- = -\frac{R_1}{R_1+R_2}V_{cc}$
    \end{itemize}

    \subsubsection{Expérimentation}%
    
    On utilise les équipements suivants :
    \begin{itemize}
        \item $R_1 = \SI{1}{\kilo\ohm}$ 
        \item $R_2 = \SI{2.2}{\kilo\ohm}$
        \item $R = \SIrange{10}{20}{\kilo\ohm}$
        \item $C = \SI{100}{\nano\F}$
    \end{itemize}
    On a donc :
    \begin{align*}
        V_{th, \text{theorique}}^\pm &= \pm \frac{1}{3.2} V_{cc} \simeq \pm \SI{4.7}{\V}
        \\
        T_{min, \text{theorique}} &= 2RC \ln\left(1+\frac{2R_1}{R_2}\right) = 2 \times 10
        \times 10^3 \times 100 \times 10^{-9} \times \ln\left(1+\frac{2}{2.2}\right) \simeq
        \SI{1.28}{\milli\s} \\
        T_{max, \text{theorique}} &= 2RC \ln\left(1+\frac{2R_1}{R_2}\right) = 2 \times 20
        \times 10^3 \times 100 \times 10^{-9} \times \ln\left(1+\frac{2}{2.2}\right) \simeq 
        \SI{2.56}{\milli\s} \\
        f_{min, \text{theorique}} &= \SI{384}{\hertz} \\
        f_{max, \text{theorique}} &= \SI{770}{\hertz} \\
    \end{align*}
    
    En pratique, on mesure :
     \begin{align*}
        f_{min} &= \SI{400}{\hertz} \\
        f_{max} &= \SI{750}{\hertz}
    \end{align*}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{2.1_Oscillo.jpg}
        \caption{Exemple de signal émis}
    \end{figure}

    \subsubsection{Conclusion}%

    Le multivibrateur astable permet, à partir d'une tension continue, de générer un
    signal périodique. On peut changer sa fréquence en changeant la valeur des résistances
    $R,R_1 $ et $R_2$.

    \subsection{Suiveur}

    On place un AOP suiveur à la suite du multivibrateur afin de faire un \og blocage \fg{}.

    En effet, si l'on plaçait une résistance directement sur la sortie du multivibrateur,
    elle court-circuiterait la capacité. Le signal observé serait alors constant.

    Le suiveur permet de faire une séparation entre le montage précédent et la résistance.

    \subsection{Sommateur avec générateur à basses fréquences}

    \subsubsection{Principes théoriques}%

    Dans cette partie, nous ajoutons un sommateur à la suite du montage précédent, afin
    d'obtenir le montage suivant :
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{2.3_Montage_Global.png}
        \caption{Montage global jusqu'ici}
    \end{figure}

    Celui-ci servira à simuler du bruit sur notre signal provenant du suiveur, grâce à un
    GBF (Générateur Basse Fréquence).
    
    Le sommateur est un montage assez simple : il utilise le principe de superposition
    afin d'additionner deux tensions et de donner le résultat en sortie.

    \subsubsection{Expérimentation}%
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.9\textwidth]{2.3_Oscillo.png}
        \caption{Différentes tensions de sortie en fonction de l'amplitude du bruit}
    \end{figure}
    
    \subsubsection{Conclusion}%

    La première partie s'achève ici. Nous avons créé le signal que nous allons ensuite
    traiter. Il s'agit d'un signal périodique, avec du bruit qui pourrait fausser le
    résultat du fréquencemètre.

    \subsection{Comparateur à hystérésis}

    \subsubsection{Principes théoriques}%

    Le comparateur à hystérésis, plus connu sous le nom de Trigger de Schmitt, ressemble à
    un simple montage amplificateur. Cependant, la contre réaction est effectuée sur la
    patte $+$, ce qui en fait un comparateur.

    Nous allons nous en servir pour transformer le signal d'entrée en un signal carré.

    Commençons par déterminer la valeur des résistances à utiliser. On fixe le niveau de
    bruit à $\SI{150}{\milli\V}$, c'est-à-dire 1\% de $V_{cc}$.

    Le trigger de Schmitt doit donc ignorer les changements inférieurs à
    $\SI{150}{\milli\V}$.

    C'est-à-dire : $V_{th}^\pm = \pm \frac{R_1'}{R_1'+R_2'} = 150$.

    On prendra donc $R_1' = \frac{1}{100} R_2'$.

    \subsubsection{Expérimentation}%

    On fixe $R_2' = \SI{33}{\kilo\ohm}$. On aura donc $R_1' = \SI{330}{\ohm}$

    Cela donne bien des valeurs de $V_{th}^\pm$ correspondant aux alentours de
    $\SI{150}{\milli\V}$ 
    \begin{figure}[H]
        \centering
        \includegraphics[width=0.5\textwidth]{2.4_Oscillo.jpg}
        \caption{Courbe Lissajous}
    \end{figure}

    En pratique, le signal émis ne contient plus de bruit, mais seulement si la
    tension $V_{pp}$ du GBF est inférieure à $\SI{150}{\milli\V}$.
    \begin{figure}[H]
        \centering
        \includegraphics[width=0.9\textwidth]{2.4_Oscillo2.png}
        \caption{Résultat selon le niveau de bruit}
    \end{figure}
    
    \subsubsection{Conclusion}%

    Le Trigger de Schmitt permet d'effectuer deux opérations : transformer un signal
    périodique quelconque en signal périodique carré, et éliminer le bruit selon un seuil
    préconfiguré.

    \subsection{Mise en forme du signal}

    \subsubsection{Principes théoriques}%
    
    \noindent\begin{minipage}{0.5\textwidth}% adapt widths of minipages to your needs
        Nous allons maintenant préparer le signal afin qu'il puisse être traité par Arduino.

        Arduino prend seulement en charge les tensions comprises entre 0 et \SI{5}{\V}. 

        Nous allons donc utiliser le montage ci-contre :
    \end{minipage}%
    \hfill%
    \begin{minipage}{0.4\textwidth}\raggedleft
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.9\textwidth]{2.5_Montage.png}
            \caption{Montage d'un transistor permettant d'obtenir un signal compatible TTL}
        \end{figure}
    \end{minipage}

    Nous obtenons ainsi le montage global suivant :
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.9\textwidth]{2.5_Montage_Global.png}
        \caption{Montage final}
    \end{figure}

    \subsubsection{Expérimentation}%
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{2.5_Oscillo.jpg}
        \caption{Signal compatible TTL}
    \end{figure}

    \subsubsection{Conclusion}%

    Grâce à un transistor, il est possible de réduire une tension à un intervalle fixé.

    \subsection{Fréquencemètre Arduino}

    \subsubsection{Principes théoriques}%
    
    Nous allons utiliser un Arduino Uno en tant que pièce finale de notre fréquencemètre.

    Le principe est simple : il va calculer la durée de l'impulsion basse, de l'impulsion
    haute et les additionner. Cela constituera une période, que l'on n'aura plus qu'à
    inverser pour obtenir une fréquence.

    Le code est le suivant :
\begin{lstlisting}
int Signal_GBF = 12; // Signal sur Entree 12

void setup() {

// Configure le port série pour l'exemple
Serial.begin(9600);

// Met la broche de signal venant du GBF en entrée
pinMode(Signal_GBF, INPUT);
}

void loop() {

// Mesure la durée de l'impulsion haute (timeout par défaut de 1s)
noInterrupts();
unsigned long etat_haut = pulseIn(Signal_GBF, HIGH);
interrupts();

// Mesure la durée de l'impulsion basse (timeout par défaut de 1s)
noInterrupts();
unsigned long etat_bas = pulseIn(Signal_GBF, LOW);
interrupts();

// Calcul de la periode = etat haut + etat bas
long periode = (etat_bas + etat_haut);
// Calcul de la frequence = 1 / periode
long frequence = (1/ (periode*0.000001));
Serial.println("Duree etat haut : ");
Serial.print(etat_haut);
Serial.println("");
Serial.println("Duree etat bas : ");
Serial.print(etat_bas);
Serial.println("");
Serial.println("Periode : ");
Serial.print(periode);
Serial.println("");
Serial.println("Frequence : ");
Serial.print(frequence);
Serial.println(" Hz");
Serial.println("");

delay(1000);
}
\end{lstlisting}

    \subsubsection{Expérimentation}%
    
    Les valeurs données semblent correspondre aux valeurs attendues. Hourra !
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{2.6_Oscillo.jpg}
        \caption{Les valeurs affichées par l'ordinateur et l'oscilloscope concordent}
    \end{figure}

    \section{Conclusion}

    Cette expérience nous a permis de mieux comprendre les difficultés qu'un ingénieur en
    électronique doit affronter.

    Par exemple, il n'y a pas de solution à tout. Il n'est pas possible de s'adapter à un
    bruit variable, on définir un seuil en deçà duquel tout changement sera considéré
    comme du bruit, et non comme une valeur valide.

    Il est aussi très difficile d'obtenir des valeurs précises. Cela serait encore plus
    vrai dans le cas d'une transmission sans fil.
\end{document}
